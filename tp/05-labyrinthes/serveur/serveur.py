from flask import Flask, render_template, request, Response, jsonify
from queue import Queue, Empty

app = Flask(__name__, static_url_path='/static')

event_queue = Queue()
largeur = None
hauteur = None
taille = None

@app.route('/')
def index():
  return app.send_static_file("index.html")

@app.route('/style.css')
def css():
  return app.send_static_file("style.css")

@app.route('/script.js')
def javascript():
  return app.send_static_file("script.js")

@app.route('/laby', methods=['GET'])
def laby():
  global hauteur
  global largeur
  global taille
  
  args = request.args
  largeur = args['largeur']
  hauteur = args['hauteur']
  taille = args['taille']

  return render_template("laby.html", largeur=largeur, hauteur=hauteur, taille=taille)

@app.route('/events')
def events():
  evts = []
  first = True
  while first or not event_queue.empty():
    if first:
      evts.extend(event_queue.get(block=True,timeout=None))
      first = False
    else:
      evts.extend(event_queue.get(block=False))
  return jsonify(evts)

@app.route('/update', methods=['POST'])
def update():
  data = request.get_json()
  event_queue.put(data)
  return jsonify(data)

@app.route('/dimensions')
def dimensions():
  return jsonify({"largeur":largeur, "hauteur":hauteur})

app.run(debug=True)