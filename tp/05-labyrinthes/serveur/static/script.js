var laby = {}

laby.init = function(largeur, hauteur, taille) {
    laby.hauteur = hauteur;
    laby.largeur = largeur;    
    laby.cellule = taille;
    laby.init_labyrinthe();
    laby.init_watch();
}

laby.init_labyrinthe = function() {
    const div = document.getElementById("laby");
    const table = div.firstElementChild;
    table.style.borderCollapse = "collapse";
    laby.cells = [];
    for (var l = 0; l < laby.hauteur; l++) {
        var tr = document.createElement("tr")
        var ligne = [];
        for (var c = 0; c < laby.largeur; c++) {
            var td = document.createElement("td");
            td.style.width = laby.cellule.toString() + "px";
            td.style.height = laby.cellule.toString() + "px";
            td.style.border = "1px solid gray";
            tr.appendChild(td);
            ligne.push(td)
        }
        table.appendChild(tr);
        laby.cells.push(ligne)
    }
}

laby.init_watch = async function() {
    //laby.timer = setInterval(laby.watch, 250);
    laby.requete_en_cours = false;
    while (true) {
        if (!laby.requete_en_cours) {
            laby.watch();
        }
        await new Promise(r => setTimeout(r, 16));
    }
}

laby.watch = function() {
    var jsonUrl = "/events";
    var requete = new XMLHttpRequest();

    laby.requete_en_cours = true;
    requete.open("GET", jsonUrl);
    requete.responseType = "json";
    requete.send();

    requete.onload = function() {
        if (requete.status == 200) {
            const json = requete.response;
            for (var i in json) {
                const data = json[i];
                const x = data[0];
                const y = data[1];
                const c = data[2];
                const td = laby.cells[y][x];
                td.style.backgroundColor = c;
            }
        }
        laby.requete_en_cours = false;
    };

}