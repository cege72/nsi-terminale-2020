import requests
import random
import time

from piles import Pile2 as Pile
from files import File2 as File

class Laby:
   def __init__(self, url):
      self._url = url

      dimensions = self._dimensions()
      self._hauteur= int(dimensions["hauteur"])
      self._largeur = int(dimensions["largeur"])

      self._data = File()

   @property
   def largeur(self):
      return self._largeur

   @property
   def hauteur(self):
      return self._hauteur

   def _dimensions(self):
      url = self._url + '/dimensions'
      r = requests.get(url)
      return r.json()

   def rectangle(self, x1, y1, x2, y2, couleur):
      for i in range(x1, x2 + 1):
         for j in range(y1, y2 + 1):
            self.peindre(i, j, couleur)
            
   def peindre(self, x, y, couleur):
      self._data.enfiler((x, y, couleur))

   def envoyer(self, ):
      data = []
      while not self._data.est_vide():
         data.append(self._data.défiler())
         
      url = self._url + '/update'
      requests.post(url, json=data)

   def fabrique_laby(self, update=False, délais=50):
      self.rectangle(0, 0, self._largeur-1, self._hauteur-1, "#555")
      
      if random.randint(0, 1) == 0:
         return self.fabrique_laby_exploration(update, délais)
      else:
         return self.fabrique_laby_fusion(update, délais)

   def fabrique_laby_exploration(self, update=False, délais=50):
      MUR = 1
      VIDE = 0

      hauteur = self._hauteur
      largeur = self._largeur

      largeur_reelle = (largeur - 1) / 2
      hauteur_reelle = (hauteur - 1) / 2

      laby = []
      for n in range(hauteur):
         ligne = [MUR]*largeur
         laby.append(ligne)

      debut = (1, 1)
      compteur = hauteur_reelle*largeur_reelle
      
      pile = Pile()
      pile.empiler(debut)
      while compteur > 0:
         if update:
            time.sleep(délais * 0.001)
            
         y, x = pile.sommet()

         if laby[y][x] == MUR:
            laby[y][x] = VIDE
            self.peindre(x, y, "#bbb")
            if update:
               self.envoyer()
            compteur = compteur - 1

         directions_possibles = []
         for dy, dx in [(1,0), (-1, 0), (0, 1), (0, -1)]:
            if ((dy == -1 and y > 1) or
                  (dy == 1 and y < hauteur - 2) or
                  (dx == -1 and x > 1) or
                  (dx == 1 and x < largeur - 2)):
                  if laby[y+2*dy][x+2*dx] == MUR:
                     directions_possibles.append((dy, dx))
                  #else:
                  #   if random.random() >= 0.95:
                  #      directions_possibles.append((dy, dx))

         if len(directions_possibles) == 0:
            pile.dépiler()
         else:
            dy, dx = random.choice(directions_possibles)
            laby[y+dy][x+dx] = VIDE
            self.peindre(x+dx, y+dy, "#bbb")
            if update:
               self.envoyer()
            pile.empiler((y+2*dy, x+2*dx))

      self.envoyer()
      return laby

   def fabrique_laby_fusion(self, update=False, délais=50):
      largeur = self._largeur
      hauteur = self._hauteur
      matrice = [[1 for _ in range(largeur)] for _ in range(hauteur)]
      if update:
         couleurs = {}
      count = -1
      groupes = {}
      murs = []
      
      def couleur_aléatoire():
         rouge = random.randint(0, 255)
         vert = random.randint(0, 255)
         bleu = random.randint(0, 255)
         couleur = "rgb({}, {}, {})"
         couleur = couleur.format(rouge, vert, bleu)
         return couleur

      def fusionne_groupes(g1, g2):
         # On récupère la couleur d'un des éléments de g1
         x, y = g1.pop()
         g1.add((x, y))
         identifiant = matrice[y][x]
         if update:
            couleur = couleurs[identifiant]
         
         for x, y in g2:
            g1.add((x, y))
            if x % 2 == 1 and y % 2 == 1:
               groupes[(x, y)] = g1
            matrice[y][x] = identifiant
            if update:
               self.peindre(x, y, couleur)
            
         return identifiant
      
      for y in range(hauteur):
         for x in range(largeur):
            self.peindre(x, y, "#555")
            if x % 2 == 1 and y % 2 == 1:
               matrice[y][x] = count
               if update:
                  couleurs[count] = couleur_aléatoire()
                  self.peindre(x, y, couleurs[count])
               groupes[(x, y)] = set([(x, y)])
               count -= 1
               
            if (x % 2) + (y % 2) == 1:
               if 0 < y < hauteur-1 and 0 < x < largeur-1:
                  murs.append((x, y))

      if update:
         self.envoyer()

      random.shuffle(murs)
      for mx, my in murs:
         # Est-ce un mur vertical ou horizontal ?
         if mx % 2 == 0:
            # vertical
            groupe1 = groupes[(mx-1, my)]
            groupe2 = groupes[(mx+1, my)]
         else:
            # horizontal
            groupe1 = groupes[(mx, my-1)]
            groupe2 = groupes[(mx, my+1)]

         if groupe1 == groupe2:
            # Un mur appartenant à un même groupe:
            # On ne peut pas le casser
            pass
         else:
            # mur appartenant à des groupes distincts:
            # On détruit le mur et on fusionne les groupes.
            id_couleur = fusionne_groupes(groupe1, groupe2)
            matrice[my][mx] = id_couleur
            if update:
               self.peindre(mx, my, couleurs[id_couleur])
               self.envoyer()
               time.sleep(délais * 0.001)
            groupe1.add((mx, my))

      for y in range(hauteur):
         for x in range(largeur):
            if matrice[y][x] < 0:
               matrice[y][x] = 0
               self.peindre(x, y, "#bbb")
      self.envoyer()
      
      return matrice
      

if __name__ == '__main__':
   lab = Laby("http://localhost:5000")
   hauteur = lab.hauteur
   largeur = lab.largeur
   print(largeur, hauteur)
   lab.rectangle(0, 0, largeur-1, hauteur-1, "black")
   lab.fabrique_laby()
