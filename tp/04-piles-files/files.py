from listes import (cellule_variable, 
                    tête, queue, 
                    liste_vide, est_vide, 
                    change_queue)

from piles import Pile2 as Pile

class File:
   """
   Je représente une file, implémentée grâce aux listes python.
   """
   
   def __init__(self):
      """
      Crée une file vide
      """
      self._tableau = []

   def est_vide(self):
      """
      Renvoie True ssi je suis une file vide.
      """
      return len(self._tableau) == 0

   def taille(self):
      """
      Renvoie ma taille (en nombre d'éléments emfilés)
      """
      return len(self._tableau)

   def enfiler(self, valeur):
      """
      Enfile un nouvel élément.
      """
      self._tableau.append(valeur)

   def défiler(self):
      """
      Défile et renvoie la valeur de l'élément placé premier
      dans la file (le plus ancien).

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Défiler sur une file vide")
      else:
         return self._tableau.pop(0)

   def consulte(self):
      """
      Renvoie la valeur de l'élément placé en position
      de défilement (la valeur la plus ancienne).

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Défiler sur une file vide")
      else:
         return self._tableau[0]

   def vider(self):
      """
      Enlève tous les éléments empiles.
      """
      self._tableau = []

   def __str__(self):
      N = self.taille()
      
      chaîne = "("
      
      i = 0
      while i < N:
         chaîne += str(self._tableau[i])
         if i < N - 1:
            chaîne += " ← "
         i += 1

      chaîne += ")"

      return chaîne

   def __repr__(self):
      return str(self)

class File2:
   """
   Je représente une file, implémentée grâce à une liste
   chaînée variable.
   """
   
   def __init__(self):
      """
      Crée une file vide
      """
      self._première = liste_vide()
      self._dernière = self._première
      self._taille = 0

   def est_vide(self):
      """
      Renvoie True ssi je suis une file vide.
      """
      return est_vide(self._première)

   def taille(self):
      """
      Renvoie ma taille (en nombre d'éléments empilés)
      """
      return self._taille

   def enfiler(self, valeur):
      """
      Enfile un nouvel élément.
      """

      # On ajoute le nouvel élément à l'arrière de la liste:
      # C'est à ça que sert l'attribut self._dernière

      nouvelle_cellule = cellule_variable(valeur, liste_vide())
      
      if est_vide(self._dernière):
         # cas particulier: première valeur. 
         self._dernière = nouvelle_cellule
         self._première = self._dernière
      else:
         # On ajoute la nouvelle cellule en bout de liste,
         # mais on ne touche pas à la première cellule qui 
         # n'est pas ici modifiée par cette opération.
         change_queue(self._dernière, nouvelle_cellule)

         # Il ne faut pas oublier d'actualiser le pointeur
         # vers la dernière cellule, qui vient juste de
         # changer.
         self._dernière = nouvelle_cellule

      self._taille += 1

   def défiler(self):
      """
      Défile et renvoie la valeur de l'élément placé premier
      dans la file (le plus ancien).

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Défiler sur une file vide")
      else:
         valeur = tête(self._première)
         self._première = queue(self._première)
         self._taille -= 1

         if est_vide(self._première):
            # On vient de retirer la dernière cellule
            self._dernière = self._première

         return valeur

   def consulte(self):
      """
      Renvoie la valeur de l'élément placé en position
      de défilement (la valeur la plus ancienne).

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Défiler sur une file vide")
      else:
         return tête(self._première)

   def vider(self):
      """
      Enlève tous les éléments empiles.
      """
      self._première = liste_vide()
      self._dernière = self._première
      self._taille = 0

   def __str__(self):
      chaîne = "("
      
      position = self._première
      while not est_vide(position):
         chaîne += str(tête(position))
         if not est_vide(queue(position)):
            chaîne += " ← "
         position = queue(position)

      chaîne += ")"

      return chaîne

   def __repr__(self):
      return str(self)

class File3:
   """
   Je représente une file, implémentée grâce à deux piles.

   Avec une analogie par les jeux de cartes:
   * la première est la "défausse", sur laquelle on empile
     les nouveaux élements que l'on souhaite mettre dans la file.
   * la deuxième est la "pioche" à partir de laquelle on peut
     dépiler les éléments que l'on souhaite retirer de la file.
   * si la "pioche" est vide, on retourne (= on inverse) la
     "défausse" pour remplir la "pioche" avant de dépiler.
   """
   
   def __init__(self):
      """
      Crée une file vide
      """
      self._entrées = Pile() # défausse
      self._sorties = Pile() # pioche

   def est_vide(self):
      """
      Renvoie True ssi je suis une file vide.
      """
      return self._entrées.est_vide() and self._sorties.est_vide()

   def taille(self):
      """
      Renvoie ma taille (en nombre d'éléments empilés)
      """
      return self._entrées.taille() + self._sorties.taille()

   def enfiler(self, valeur):
      """
      Enfile un nouvel élément.
      """

      self._entrées.empiler(valeur)


   def _retourne_piles(self):
      """
      Renverse la pile des entrées vers les sorties.
      
      Usage interne uniquement.
      """
      while not self._entrées.est_vide():
         self._sorties.empiler(self._entrées.dépiler())

   def défiler(self):
      """
      Défile et renvoie la valeur de l'élément placé premier
      dans la file (le plus ancien).

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Défiler sur une pile vide")
      else:
         if self._sorties.est_vide():
            # La pile des valeurs sortantes ("pioche") est
            # vide: on retourne la pile des valeurs entrantes
            # ("défausse"):
            self._retourne_piles()
         
         # On dépile la valeur à renvoyer
         return self._sorties.dépiler()

   def consulte(self):
      """
      Renvoie la valeur de l'élément placé en position
      de défilement (la valeur la plus ancienne).

      Déclenche une erreur si je suis vide.
      """
      if self.est_vide():
         raise IndexError("Défiler sur une file vide")
      else:
         if self._sorties.est_vide():
            # La pile des valeurs sortantes ("pioche") est
            # vide: on retourne la pile des valeurs entrantes
            # ("défausse"):
            self._retourne_piles()

         return self._sorties.sommet()

   def vider(self):
      """
      Enlève tous les éléments empiles.
      """
      self._entrées.vider()
      self._sorties.vider()

   def __str__(self):
      # On retourne les entrées vers les sorties, afin
      # que tous les éléments de la file soient dans la
      # pile des sorties: plus facile pour afficher ensuite.
      #
      # Le fait d'afficher la file perturbe totalement sa
      # structure, mais ce n'est pas bien gênant: on n'affiche
      # en général une file que lorsque l'on met au point notre
      # programme, pas lorsqu'il fonctionne correctement.
      self._retourne_piles()

      # Comme on ne peut pas accéder aux éléments d'une pile
      # (sauf son sommet), on va ruser: on commencer par retourner
      # la pile des sorties dans une pile temporaire (ce qui va
      # vider les sorties), tout en affichange les valeurs
      # recueillies.
      chaîne = "("
      
      temporaire = Pile()
      while not self._sorties.est_vide():
         valeur = self._sorties.dépiler()
         temporaire.empiler(valeur)
         chaîne += str(valeur)
         if self._sorties.taille() > 0:
            chaîne += " ← "

      # Puis on retourne à nouveau cette pile temporaire 
      # dans la pile des sorties.
      #
      # Le fait de faire un double retournement est certes
      # peu efficace et économique, mais cela remet bien les
      # éléments dans le bon ordre.
      #
      # Il existe bien évidemment des moyens bien plus pratiques
      # pour réaliser cela en python, mais ils sont hors-programme
      # en NSI.
      
      while not temporaire.est_vide():
         self._sorties.empiler(temporaire.dépiler())

      chaîne += ")"

      return chaîne

   def __repr__(self):
      return str(self)

