from random import shuffle
from sys import stdout

from cartes import Carte
from files import File2 as File
from piles import Pile2 as Pile

def bataille(affichage=True):
   # Création et mélange du deck de 52 cartes
   liste = []
   for hauteur in Carte.noms_cartes:
      for couleur in Carte.noms_couleurs:
         liste.append(Carte(hauteur, couleur))
   shuffle(liste)

   # Distribution alternative aux 2 joueurs
   joueur1 = File()
   joueur2 = File()
   stock = Pile()

   for i in range(52):
      if i % 2 == 0:
         joueur1.enfiler(liste[i])
      else:
         joueur2.enfiler(liste[i])

   #print("Joueur 1:", joueur1)
   #print("Joueur 2:", joueur2)

   compteur = 0
   # Jeu de bataille
   while joueur1.taille() > 0 and joueur2.taille() > 0:
      compteur += 1
      assert stock.est_vide()
      assert joueur1.taille() + joueur2.taille() == 52

      # On commence le tour
      c1 = joueur1.défiler()
      c2 = joueur2.défiler()
      stock.empiler(c1)
      stock.empiler(c2)
      if affichage:
         chaîne = "Tour {} (A:{}/B:{}): Joueur A tire {} - Joueur B tire {}"
         chaîne.format(compteur, joueur1.taille(), joueur2.taille(), c1, c2)
         print(chaîne)

      if c1 == c2:
         # Bataille !
         if affichage:
            print("        Bataille: ", end="")
         while c1 == c2:
            if affichage:
               print("*", end="")

            if joueur1.taille() < 2 or joueur2.taille() < 2:
               if affichage:
                  print("        Match nul")
               return 0, compteur # Match nul
            
            stock.empiler(joueur1.défiler())
            stock.empiler(joueur2.défiler())

            c1 = joueur1.défiler()
            c2 = joueur2.défiler()
            stock.empiler(c1)
            stock.empiler(c2)

         if affichage:
            print()
            
      if affichage:
         if c1 > c2:
            print("        Joueur A gagne {} cartes".format(stock.taille()))
         else:
            print("        Joueur B gagne {} cartes".format(stock.taille()))

      # On mélange toutes les cartes à remettre au vainqueur
      deck = []            
      while stock.taille() > 0:
         deck.append(stock.dépiler())

      shuffle(deck)

      for c in deck:
         if c1 > c2:
            joueur1.enfiler(c)
         else:
            joueur2.enfiler(c)

   # Le jeu est terminé
   if joueur1.taille() == 0:
      return 2, compteur
   else:
      return 1, compteur

N = 100000
résultats = {}
for r in [0, 1, 2]:
   résultats[r] = []

somme = 0
for i in range(N):
   r, c = bataille(affichage=False)
   résultats[r].append(c)
   somme += c
   if i % (N // 100) == 0:
      print(".", end="")
      stdout.flush()
print()

#print(résultats)
print("Moyenne={}".format(somme / N))