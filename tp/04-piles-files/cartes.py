class Carte:
   noms_cartes = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "valet", "dame", "roi", "as"]
   noms_couleurs = ["coeur", "carreau", "pique", "trèfle"]
   noms_cartes_str = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "V", "D", "R", "A"]
   noms_couleurs_str = ["♡", "♢", "♠", "♣"]
   
   def __init__(self, hauteur, couleur):
      assert hauteur in Carte.noms_cartes, "Hauteur invalide"
      assert couleur in Carte.noms_couleurs, "Couleur invalide"
      
      self._hauteur = Carte.noms_cartes.index(hauteur)
      self._couleur = Carte.noms_couleurs.index(couleur)
      
   def hauteur(self):
      return Carte.noms_cartes[self._hauteur]
   
   def couleur(self):
      return Carte.noms_couleurs[self._couleur]

   def __lt__(self, other):
      return self._hauteur < other._hauteur
   
   def __le__(self, other):
      return self._hauteur <= other._hauteur
   
   def __eq__(self, other):
      return self._hauteur == other._hauteur
   
   def __str__(self):
      return "{}{}".format(Carte.noms_cartes_str[self._hauteur], 
                           Carte.noms_couleurs_str[self._couleur])
   
   def __repr__(self):
      return "Carte({},{})".format(repr(self.hauteur()), repr(self.couleur()))
   
